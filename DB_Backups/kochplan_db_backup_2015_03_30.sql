-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: kochplan_atze
-- ------------------------------------------------------
-- Server version	5.6.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `child`
--

DROP TABLE IF EXISTS `child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `child` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `family_id` varchar(255) NOT NULL,
  `join_date` date NOT NULL,
  `left_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `child_family_id` (`family_id`),
  CONSTRAINT `child_ibfk_1` FOREIGN KEY (`family_id`) REFERENCES `family` (`family`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `child`
--

LOCK TABLES `child` WRITE;
/*!40000 ALTER TABLE `child` DISABLE KEYS */;
INSERT INTO `child` VALUES (1,'Valentina','Weber','2014-07-01','2099-01-01'),(2,'Paula','Riedel','2014-07-01','2099-01-01'),(3,'Theo','Ehmann','2014-07-01','2099-01-01'),(4,'Joas','Tolonen','2014-07-01','2099-01-01'),(5,'Lotta','Geffert','2014-07-01','2099-01-01'),(6,'Eeli','Tolonen','2014-07-01','2099-01-01'),(7,'Elio','Nussbaum','2014-07-01','2099-01-01'),(8,'Penelope','Baetz','2014-07-01','2099-01-01'),(9,'Marine','Moreau','2014-07-01','2099-01-01'),(10,'Rosa','de_Roux','2014-07-01','2099-01-01'),(11,'Jacek','Stasiak','2013-07-01','2099-01-01'),(12,'Helena','Stasiak','2014-07-01','2099-01-01'),(13,'Johannes','Gregor','2014-07-01','2099-01-01'),(14,'Hanna','Roloff','2014-07-01','2099-01-01'),(15,'Tim','Roloff','2014-07-01','2099-01-01'),(16,'NOT_ASSIGNED','NOT_ASSIGNED','2014-07-01','2099-12-31');
/*!40000 ALTER TABLE `child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `childserviceassignment`
--

DROP TABLE IF EXISTS `childserviceassignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `childserviceassignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `child_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `childserviceassignment_child_id` (`child_id`),
  KEY `childserviceassignment_service_id` (`service_id`),
  CONSTRAINT `childserviceassignment_ibfk_1` FOREIGN KEY (`child_id`) REFERENCES `child` (`id`),
  CONSTRAINT `childserviceassignment_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=513 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `childserviceassignment`
--

LOCK TABLES `childserviceassignment` WRITE;
/*!40000 ALTER TABLE `childserviceassignment` DISABLE KEYS */;
INSERT INTO `childserviceassignment` VALUES (315,16,3,'2014-09-04'),(344,16,1,'2014-09-01'),(345,16,1,'2014-09-02'),(346,16,2,'2014-09-03'),(347,16,1,'2014-09-05'),(348,16,1,'2014-09-08'),(349,16,1,'2014-09-09'),(350,16,2,'2014-09-10'),(351,16,1,'2014-09-11'),(352,16,1,'2014-09-12'),(353,16,1,'2014-09-15'),(354,16,1,'2014-09-16'),(355,16,2,'2014-09-17'),(356,16,1,'2014-09-18'),(357,16,3,'2014-09-19'),(358,16,1,'2014-09-22'),(359,16,1,'2014-09-23'),(360,16,2,'2014-09-24'),(361,16,1,'2014-09-25'),(362,16,1,'2014-09-26'),(363,16,1,'2014-09-29'),(364,16,1,'2014-09-30'),(365,10,2,'2014-10-01'),(366,8,3,'2014-10-02'),(367,3,1,'2014-10-06'),(368,5,1,'2014-10-07'),(369,11,2,'2014-10-08'),(370,12,1,'2014-10-09'),(371,13,1,'2014-10-10'),(372,9,1,'2014-10-13'),(373,7,1,'2014-10-14'),(374,8,2,'2014-10-15'),(375,14,1,'2014-10-16'),(376,10,3,'2014-10-17'),(377,4,1,'2014-10-20'),(378,1,1,'2014-10-21'),(379,2,2,'2014-10-22'),(380,15,1,'2014-10-23'),(381,8,1,'2014-10-24'),(382,6,1,'2014-10-27'),(383,14,1,'2014-10-28'),(384,13,2,'2014-10-29'),(385,10,1,'2014-10-30'),(388,3,3,'2014-10-31'),(389,11,1,'2014-11-03'),(390,12,1,'2014-11-04'),(391,9,2,'2014-11-05'),(392,2,1,'2014-11-06'),(393,7,1,'2014-11-07'),(394,13,1,'2014-11-10'),(395,4,1,'2014-11-11'),(396,3,2,'2014-11-12'),(397,1,1,'2014-11-13'),(398,5,3,'2014-11-14'),(399,9,1,'2014-11-17'),(400,6,1,'2014-11-18'),(401,7,2,'2014-11-19'),(402,8,1,'2014-11-20'),(403,10,1,'2014-11-21'),(404,3,1,'2014-11-24'),(405,2,1,'2014-11-25'),(406,5,2,'2014-11-26'),(407,11,3,'2014-11-27'),(408,12,1,'2014-11-28'),(409,7,1,'2014-12-01'),(410,4,1,'2014-12-02'),(411,14,2,'2014-12-03'),(412,1,1,'2014-12-04'),(413,9,1,'2014-12-05'),(414,5,1,'2014-12-08'),(415,6,1,'2014-12-09'),(416,15,2,'2014-12-10'),(417,13,3,'2014-12-11'),(418,8,1,'2014-12-12'),(419,10,1,'2014-12-15'),(420,3,1,'2014-12-16'),(421,12,2,'2014-12-17'),(422,11,1,'2014-12-18'),(423,2,1,'2014-12-19'),(424,9,3,'2015-01-05'),(425,15,1,'2015-01-06'),(426,4,2,'2015-01-07'),(427,13,1,'2015-01-08'),(428,1,1,'2015-01-09'),(429,5,1,'2015-01-12'),(430,14,1,'2015-01-13'),(431,6,2,'2015-01-14'),(432,12,1,'2015-01-15'),(433,11,1,'2015-01-16'),(434,7,3,'2015-01-19'),(435,4,1,'2015-01-20'),(436,1,2,'2015-01-21'),(437,9,1,'2015-01-22'),(438,8,1,'2015-01-23'),(439,3,1,'2015-01-26'),(440,6,1,'2015-01-27'),(441,10,2,'2015-01-28'),(442,2,1,'2015-01-29'),(443,13,1,'2015-01-30'),(444,1,1,'2015-02-02'),(445,14,3,'2015-02-03'),(446,9,2,'2015-02-04'),(447,5,1,'2015-02-05'),(448,4,1,'2015-02-06'),(449,3,1,'2015-02-09'),(450,15,1,'2015-02-10'),(451,11,2,'2015-02-11'),(452,12,1,'2015-02-12'),(453,6,1,'2015-02-13'),(454,2,3,'2015-02-16'),(455,7,1,'2015-02-17'),(456,8,2,'2015-02-18'),(457,10,1,'2015-02-19'),(458,14,1,'2015-02-20'),(459,11,1,'2015-02-23'),(460,12,1,'2015-02-24'),(461,13,2,'2015-02-25'),(462,9,1,'2015-02-26'),(463,15,1,'2015-02-27'),(464,8,1,'2015-03-02'),(465,1,3,'2015-03-03'),(466,2,2,'2015-03-04'),(467,5,1,'2015-03-05'),(468,7,1,'2015-03-06'),(469,13,1,'2015-03-09'),(470,10,1,'2015-03-10'),(471,3,2,'2015-03-11'),(472,4,1,'2015-03-12'),(473,9,1,'2015-03-13'),(474,2,1,'2015-03-16'),(475,15,3,'2015-03-17'),(476,7,2,'2015-03-18'),(477,6,1,'2015-03-19'),(478,1,1,'2015-03-20'),(486,11,1,'2015-03-23'),(487,12,1,'2015-03-24'),(488,5,2,'2015-03-25'),(489,8,1,'2015-03-26'),(490,13,1,'2015-03-27'),(491,3,3,'2015-03-30'),(492,7,1,'2015-03-31'),(493,9,2,'2015-04-01'),(494,3,1,'2015-04-02'),(495,5,1,'2015-04-07'),(496,14,2,'2015-04-08'),(497,13,1,'2015-04-09'),(498,14,1,'2015-04-10'),(499,4,3,'2015-04-13'),(500,10,1,'2015-04-14'),(501,15,2,'2015-04-15'),(502,9,1,'2015-04-16'),(503,2,1,'2015-04-17'),(504,6,1,'2015-04-20'),(505,8,1,'2015-04-21'),(506,1,2,'2015-04-22'),(507,7,1,'2015-04-23'),(508,3,1,'2015-04-24'),(509,12,3,'2015-04-27'),(510,11,1,'2015-04-28'),(511,10,2,'2015-04-29'),(512,5,1,'2015-04-30');
/*!40000 ALTER TABLE `childserviceassignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `childservicedeactivation`
--

DROP TABLE IF EXISTS `childservicedeactivation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `childservicedeactivation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `child_id` int(11) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `childservicedeactivation_service_id` (`category_id`),
  KEY `childservicedeactivation_child_id` (`child_id`),
  CONSTRAINT `childservicedeactivation_ibfk_1` FOREIGN KEY (`child_id`) REFERENCES `child` (`id`),
  CONSTRAINT `childservicedeactivation_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `servicecategory` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `childservicedeactivation`
--

LOCK TABLES `childservicedeactivation` WRITE;
/*!40000 ALTER TABLE `childservicedeactivation` DISABLE KEYS */;
/*!40000 ALTER TABLE `childservicedeactivation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `family`
--

DROP TABLE IF EXISTS `family`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `family` (
  `family` varchar(255) NOT NULL,
  `family_name` varchar(255) NOT NULL,
  `desired_interval` int(11) NOT NULL,
  PRIMARY KEY (`family`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `family`
--

LOCK TABLES `family` WRITE;
/*!40000 ALTER TABLE `family` DISABLE KEYS */;
INSERT INTO `family` VALUES ('Baetz','Baetz',1),('de_Roux','de Roux',1),('Ehmann','Ehmann',1),('Geffert','Geffert',1),('Gregor','Gregor',1),('Moreau','Moreau',1),('NOT_ASSIGNED','NOT_ASSIGNED',1),('Nussbaum','Nussbaum',1),('Riedel','Riedel',1),('Roloff','Roloff',7),('Stasiak','Stasiak',1),('Tolonen','Tolonen',7),('Weber','Weber',1);
/*!40000 ALTER TABLE `family` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familyvacation`
--

DROP TABLE IF EXISTS `familyvacation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `familyvacation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `family_id` varchar(255) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `familyvacation_family_id` (`family_id`),
  CONSTRAINT `familyvacation_ibfk_1` FOREIGN KEY (`family_id`) REFERENCES `family` (`family`)
) ENGINE=InnoDB AUTO_INCREMENT=300 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `familyvacation`
--

LOCK TABLES `familyvacation` WRITE;
/*!40000 ALTER TABLE `familyvacation` DISABLE KEYS */;
INSERT INTO `familyvacation` VALUES (1,'Stasiak','2014-09-04'),(2,'Stasiak','2014-09-05'),(3,'Stasiak','2014-10-20'),(4,'Stasiak','2014-10-21'),(5,'Stasiak','2014-10-22'),(6,'Stasiak','2014-10-23'),(7,'Stasiak','2014-10-24'),(8,'Stasiak','2014-10-25'),(9,'Stasiak','2014-10-26'),(34,'Stasiak','2014-10-19'),(35,'Stasiak','2014-10-20'),(36,'Stasiak','2014-10-21'),(37,'Stasiak','2014-10-22'),(38,'Stasiak','2014-10-23'),(39,'Stasiak','2014-10-24'),(40,'Stasiak','2014-10-25'),(41,'Stasiak','2014-10-26'),(42,'Stasiak','2014-10-27'),(43,'Stasiak','2014-10-28'),(44,'Stasiak','2014-10-29'),(45,'Stasiak','2014-10-30'),(46,'Stasiak','2014-10-31'),(49,'Geffert','2014-10-02'),(50,'Geffert','2014-10-03'),(51,'Geffert','2014-10-19'),(52,'Geffert','2014-10-20'),(53,'Geffert','2014-10-21'),(54,'Geffert','2014-10-22'),(55,'Geffert','2014-10-23'),(56,'Geffert','2014-10-24'),(57,'Geffert','2014-10-25'),(58,'Geffert','2014-10-26'),(59,'Geffert','2014-10-27'),(60,'Geffert','2014-10-28'),(61,'Geffert','2014-10-29'),(62,'Geffert','2014-10-30'),(63,'Geffert','2014-10-31'),(64,'Geffert','2014-11-01'),(65,'Geffert','2014-11-02'),(66,'Nussbaum','2014-10-19'),(67,'Nussbaum','2014-10-20'),(68,'Nussbaum','2014-10-21'),(69,'Nussbaum','2014-10-22'),(70,'Nussbaum','2014-10-23'),(71,'Nussbaum','2014-10-24'),(72,'Nussbaum','2014-10-25'),(73,'Nussbaum','2014-10-26'),(74,'Nussbaum','2014-10-27'),(75,'Nussbaum','2014-10-28'),(76,'Nussbaum','2014-10-29'),(77,'Nussbaum','2014-10-30'),(78,'Nussbaum','2014-10-31'),(79,'Nussbaum','2014-11-01'),(80,'Nussbaum','2014-11-02'),(81,'Moreau','2014-10-23'),(82,'Moreau','2014-10-24'),(83,'Moreau','2014-10-25'),(84,'Moreau','2014-10-26'),(85,'Moreau','2014-10-27'),(86,'Moreau','2014-10-28'),(87,'Gregor','2014-10-20'),(88,'Gregor','2014-10-21'),(89,'Roloff','2014-11-18'),(90,'Roloff','2014-11-19'),(91,'Roloff','2014-11-20'),(92,'Roloff','2014-11-21'),(93,'Roloff','2014-11-22'),(94,'Roloff','2014-11-23'),(95,'Roloff','2014-11-24'),(96,'Roloff','2014-11-25'),(97,'Nussbaum','2014-12-22'),(98,'Nussbaum','2014-12-23'),(99,'Nussbaum','2014-12-24'),(100,'Nussbaum','2014-12-25'),(101,'Nussbaum','2014-12-26'),(102,'Nussbaum','2014-12-27'),(103,'Nussbaum','2014-12-28'),(104,'Nussbaum','2014-12-29'),(105,'Nussbaum','2014-12-30'),(106,'Nussbaum','2014-12-31'),(107,'Nussbaum','2015-01-01'),(108,'Nussbaum','2015-01-02'),(109,'Nussbaum','2015-01-03'),(110,'Nussbaum','2015-01-04'),(111,'Stasiak','2014-12-22'),(112,'Stasiak','2014-12-23'),(113,'Stasiak','2014-12-24'),(114,'Stasiak','2014-12-25'),(115,'Stasiak','2014-12-26'),(116,'Stasiak','2014-12-27'),(117,'Stasiak','2014-12-28'),(118,'Stasiak','2014-12-29'),(119,'Stasiak','2014-12-30'),(120,'Stasiak','2014-12-31'),(121,'Stasiak','2015-01-01'),(122,'Stasiak','2015-01-02'),(123,'Stasiak','2015-01-03'),(124,'Stasiak','2015-01-04'),(125,'Weber','2014-12-22'),(126,'Weber','2014-12-23'),(127,'Weber','2014-12-24'),(128,'Weber','2014-12-25'),(129,'Weber','2014-12-26'),(130,'Weber','2014-12-27'),(131,'Weber','2014-12-28'),(132,'Weber','2014-12-29'),(133,'Weber','2014-12-30'),(134,'Weber','2014-12-31'),(135,'Weber','2015-01-01'),(136,'Weber','2015-01-02'),(137,'Weber','2015-01-03'),(138,'Weber','2015-01-04'),(139,'Stasiak','2015-01-31'),(140,'Stasiak','2015-02-01'),(141,'Stasiak','2015-02-02'),(142,'Stasiak','2015-02-03'),(143,'Stasiak','2015-02-04'),(144,'Stasiak','2015-02-05'),(145,'Stasiak','2015-02-06'),(146,'Stasiak','2015-02-07'),(147,'Stasiak','2015-02-08'),(148,'de_Roux','2015-01-31'),(149,'de_Roux','2015-02-01'),(150,'de_Roux','2015-02-02'),(151,'de_Roux','2015-02-03'),(152,'de_Roux','2015-02-04'),(153,'de_Roux','2015-02-05'),(154,'de_Roux','2015-02-06'),(155,'de_Roux','2015-02-07'),(156,'de_Roux','2015-02-08'),(157,'Baetz','2015-01-31'),(158,'Baetz','2015-02-01'),(159,'Baetz','2015-02-02'),(160,'Baetz','2015-02-03'),(161,'Baetz','2015-02-04'),(162,'Baetz','2015-02-05'),(163,'Baetz','2015-02-06'),(164,'Baetz','2015-02-07'),(165,'Baetz','2015-02-08'),(166,'Nussbaum','2015-01-30'),(167,'Nussbaum','2015-01-31'),(168,'Nussbaum','2015-02-01'),(169,'Nussbaum','2015-02-02'),(170,'Nussbaum','2015-02-03'),(171,'Nussbaum','2015-02-04'),(172,'Nussbaum','2015-02-05'),(173,'Nussbaum','2015-02-06'),(174,'Nussbaum','2015-02-07'),(175,'Nussbaum','2015-02-08'),(185,'Stasiak','2015-03-30'),(186,'Stasiak','2015-03-31'),(187,'Stasiak','2015-04-01'),(188,'Stasiak','2015-04-02'),(189,'Stasiak','2015-04-03'),(190,'Stasiak','2015-04-04'),(191,'Stasiak','2015-04-05'),(192,'Stasiak','2015-04-06'),(193,'Stasiak','2015-04-07'),(194,'Stasiak','2015-04-08'),(195,'Stasiak','2015-04-09'),(196,'Stasiak','2015-04-10'),(197,'Stasiak','2015-04-11'),(198,'Stasiak','2015-04-12'),(199,'Stasiak','2015-04-13'),(200,'Stasiak','2015-04-14'),(201,'Roloff','2015-03-05'),(202,'Roloff','2015-03-06'),(203,'Roloff','2015-03-07'),(204,'Roloff','2015-03-08'),(205,'Roloff','2015-03-09'),(206,'Tolonen','2015-03-02'),(207,'Tolonen','2015-03-03'),(208,'Tolonen','2015-03-04'),(209,'Tolonen','2015-03-05'),(210,'Tolonen','2015-03-06'),(211,'Tolonen','2015-03-07'),(212,'Tolonen','2015-03-08'),(213,'Tolonen','2015-03-30'),(214,'Tolonen','2015-03-31'),(215,'Tolonen','2015-04-01'),(216,'Tolonen','2015-04-02'),(217,'Tolonen','2015-04-03'),(218,'Tolonen','2015-04-04'),(219,'Tolonen','2015-04-05'),(220,'Tolonen','2015-04-06'),(221,'Baetz','2015-03-30'),(222,'Baetz','2015-03-31'),(223,'Baetz','2015-04-01'),(224,'Baetz','2015-04-02'),(225,'Baetz','2015-04-03'),(226,'Baetz','2015-04-04'),(227,'Baetz','2015-04-05'),(228,'Baetz','2015-04-06'),(229,'Baetz','2015-04-07'),(230,'Baetz','2015-04-08'),(231,'Baetz','2015-04-09'),(232,'Baetz','2015-04-10'),(233,'Baetz','2015-04-11'),(234,'Baetz','2015-04-12'),(235,'Weber','2015-03-06'),(236,'Weber','2015-03-30'),(237,'Weber','2015-03-31'),(238,'Weber','2015-04-01'),(239,'Weber','2015-04-02'),(240,'Weber','2015-04-03'),(241,'Weber','2015-04-04'),(242,'Weber','2015-04-05'),(243,'Weber','2015-04-06'),(244,'Weber','2015-04-07'),(245,'Weber','2015-04-08'),(246,'Weber','2015-04-09'),(247,'Weber','2015-04-10'),(248,'Weber','2015-04-11'),(249,'Weber','2015-04-12'),(250,'Weber','2015-04-13'),(251,'Weber','2015-04-14'),(252,'Weber','2015-04-15'),(253,'de_Roux','2015-03-30'),(254,'de_Roux','2015-03-31'),(255,'de_Roux','2015-04-01'),(256,'de_Roux','2015-04-02'),(257,'de_Roux','2015-04-03'),(258,'de_Roux','2015-04-04'),(259,'de_Roux','2015-04-05'),(260,'de_Roux','2015-04-06'),(261,'de_Roux','2015-04-07'),(262,'de_Roux','2015-04-08'),(263,'de_Roux','2015-04-09'),(264,'de_Roux','2015-04-10'),(265,'de_Roux','2015-04-11'),(266,'de_Roux','2015-04-12'),(267,'Moreau','2015-03-16'),(268,'Moreau','2015-03-30'),(269,'Weber','2015-04-30'),(270,'Weber','2015-05-01'),(271,'Weber','2015-05-02'),(272,'Weber','2015-05-03'),(273,'Weber','2015-05-04'),(274,'Weber','2015-05-05'),(275,'Riedel','2015-04-03'),(276,'Riedel','2015-04-04'),(277,'Riedel','2015-04-05'),(278,'Riedel','2015-04-06'),(279,'Riedel','2015-04-07'),(280,'Riedel','2015-04-08'),(281,'Riedel','2015-04-09'),(282,'Riedel','2015-04-10'),(283,'Nussbaum','2015-04-01'),(284,'Nussbaum','2015-04-02'),(285,'Nussbaum','2015-04-03'),(286,'Nussbaum','2015-04-04'),(287,'Nussbaum','2015-04-05'),(288,'Nussbaum','2015-04-06'),(289,'Nussbaum','2015-04-07'),(290,'Nussbaum','2015-04-08'),(291,'Nussbaum','2015-04-09'),(292,'Nussbaum','2015-04-10'),(293,'Nussbaum','2015-04-11'),(294,'Nussbaum','2015-04-12'),(295,'Moreau','2015-04-09'),(296,'Moreau','2015-04-10'),(297,'Roloff','2015-04-01'),(298,'Roloff','2015-04-02'),(299,'Roloff','2015-04-24');
/*!40000 ALTER TABLE `familyvacation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noserviceday`
--

DROP TABLE IF EXISTS `noserviceday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noserviceday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `noserviceday_day` (`day`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noserviceday`
--

LOCK TABLES `noserviceday` WRITE;
/*!40000 ALTER TABLE `noserviceday` DISABLE KEYS */;
INSERT INTO `noserviceday` VALUES (1,'2014-10-03'),(2,'2014-12-22'),(3,'2014-12-23'),(4,'2014-12-24'),(5,'2014-12-25'),(6,'2014-12-26'),(7,'2014-12-27'),(8,'2014-12-28'),(9,'2014-12-29'),(10,'2014-12-30'),(11,'2014-12-31'),(12,'2015-01-01'),(13,'2015-01-02'),(14,'2015-01-03'),(15,'2015-01-04'),(16,'2015-04-03'),(17,'2015-04-06');
/*!40000 ALTER TABLE `noserviceday` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `no_servants` int(11) NOT NULL,
  `valid_on` int(11) NOT NULL,
  `interval_max` int(11) NOT NULL,
  `interval_min` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service_category_id` (`category_id`),
  CONSTRAINT `service_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `servicecategory` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'cooking_normal','cooking',1,31,1,1),(2,'cooking_meat','cooking',1,4,9,5),(3,'cooking_fish','cooking',1,31,16,12),(4,'clean_kita','cleaning',1,32,7,7),(5,'do_washing','laundry',1,32,7,7);
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicecategory`
--

DROP TABLE IF EXISTS `servicecategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicecategory` (
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicecategory`
--

LOCK TABLES `servicecategory` WRITE;
/*!40000 ALTER TABLE `servicecategory` DISABLE KEYS */;
INSERT INTO `servicecategory` VALUES ('cleaning'),('cooking'),('laundry');
/*!40000 ALTER TABLE `servicecategory` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-30 10:01:46
