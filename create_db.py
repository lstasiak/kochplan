import db_model

db_model.Family.create_table()
db_model.FamilyVacation.create_table()
db_model.Child.create_table()
db_model.NoServiceDay.create_table()
db_model.ServiceCategory.create_table()
db_model.Service.create_table()
db_model.ChildServiceDeactivation.create_table()
db_model.ChildServiceAssignment.create_table()
