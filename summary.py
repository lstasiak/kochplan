# -*- coding: ascii -*-
import argparse
import calendar
import getopt
import locale
import logging
import random
import sys

from calendar import month_name, day_name, day_abbr
from operator import itemgetter

from reportlab.graphics.shapes import Drawing
from reportlab.lib import colors
from reportlab.lib.pagesizes import *
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch
from reportlab.platypus import Paragraph, SimpleDocTemplate, Table, TableStyle, Image

from peewee import *
from db_model import *

__author__ = "Lukasz Stasiak"
__contact__ = ""

#: logger instance
logger = None


def generate_summary(start_date, end_date):

    logger.info('Service summary for period from ' + start_date.strftime('%Y-%m-%d') + ' to ' + end_date.strftime('%Y-%m-%d'))

    children = Child.select().join(Family).where(Family.family_name!='NOT_ASSIGNED') # all 'real' children
    assignments_in_period = ChildServiceAssignment.select().where(ChildServiceAssignment.date >= start_date, ChildServiceAssignment.date <= end_date) # all assignments in given period
    # currently we are interested only in 'cooking' category
    cooking_category = ServiceCategory.select().where(ServiceCategory.name=='cooking').first()
    services = cooking_category.services.order_by(Service.name) # all cooking services
    
    logger.info('Child;Fish;Meat;Normal;Cooking Total;Vacation (on "service days" only)')
    
    for child in children:
        service_cntrs = [] # counters for given services
        vacation_days = 0
        for_child_assignments = assignments_in_period.select().where(ChildServiceAssignment.child == child)
        
        # get counter of each single service
        for service in services:
            counter = for_child_assignments.select().where(ChildServiceAssignment.service == service).count()
            service_cntrs.append(counter)
            # logger.info('Child {}, service {}, # {}'.format(child.name, service.name, counter))
        
        vacation_days_in_period = FamilyVacation.select().where(FamilyVacation.family == child.family,
                                                                FamilyVacation.date >= start_date,
                                                                FamilyVacation.date <= end_date)
        
        # ignore those vacation days which were planned on a 'no service day' (assume that if there is no single service assignment
        # for a given day, it was a 'no service day', i.e. KiTa closed or weekend)
        no_service_day_cntr = 0 # no service days within vacations        
        for vacation in vacation_days_in_period:
            if ChildServiceAssignment.select().where(ChildServiceAssignment.date == vacation.date).first() == None:
                no_service_day_cntr = no_service_day_cntr + 1
        # logger.info('Child {} having {} vacation days, in which {} days were no service days.'.format(child.name, vacation_days_in_period.count(), no_service_day_cntr))
        # logger.info('{};{};{};{};{};{}'.format(child.name+' '+child.family.family_name, 
        logger.info('{};{};{};{};{};{}'.format(child.name, 
                                            service_cntrs[0], 
                                            service_cntrs[1], 
                                            service_cntrs[2],
                                            sum(service_cntrs),
                                            (vacation_days_in_period.count()-no_service_day_cntr)))
            

    no_all_cooking_days = assignments_in_period.select().join(Service).where(Service.category==cooking_category).count()
    logger.info('{};{}'.format('Number of all cooking assignments in period (~no. of working days, cleaning/washing excluded)', no_all_cooking_days))

#//////////////////////////////////////////////////////////////////////////////
def main(argv):
    global logger

    argParser = argparse.ArgumentParser(description='')
    argParser.add_argument('-s', '--start_date', dest='start_date', help="start date for the summary period (format YYYY-MM-DD)")
    argParser.add_argument('-e', '--end_date', dest='end_date', help="end date for the summary period (format YYYY-MM-DD)")
    argParser.add_argument('-l', '--loglevel', dest='loglevel', default='INFO', help='logging level: INFO, DEBUG, ERROR; default: INFO')
    args = argParser.parse_args()

    initLogger(args.loglevel)
    logger = logging.getLogger('')

    # Print infos on incomplete command
    if not (args.start_date and args.end_date):
        logger.info('Summary period not correctly defined. Exiting.')
        sys.exit(2)

    date_from = datetime.datetime.strptime(args.start_date, '%Y-%m-%d')
    date_to = datetime.datetime.strptime(args.end_date, '%Y-%m-%d')
    generate_summary(date_from, date_to)


def initLogger(loglevel):
    '''
    intialize the logger with loglevel etc

    :param loglevel: read the name of the variable
    '''
    filename = '{0}.log'.format(datetime.datetime.now().strftime("%Y%m%d_%H%M"))
    logging.basicConfig(filename=filename,
                        filemode='w',
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%d.%m %H:%M',
                        level=loglevel)

    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)


#//////////////////////////////////////////////////////////////////////////////
if __name__ == "__main__":
    main(sys.argv)
