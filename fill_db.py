import peewee
from peewee import *
import db_model
from db_model import *
import datetime

Family.insert_many([
    # {'family':'NOT_ASSIGNED', 'family_name':'NOT_ASSIGNED', 'desired_interval':'1'},   # 'empty' family
    # {'family':'Weber',        'family_name':'Weber',        'desired_interval':'1'},
    # {'family':'Riedel',       'family_name':'Riedel',       'desired_interval':'1'},
    # {'family':'Ehmann',       'family_name':'Ehmann',       'desired_interval':'1'},
    # {'family':'Tolonen',      'family_name':'Tolonen',      'desired_interval':'7'},
    # {'family':'Geffert',      'family_name':'Geffert',      'desired_interval':'1'},
    # {'family':'Nussbaum',     'family_name':'Nussbaum',     'desired_interval':'1'},
    # {'family':'Baetz',        'family_name':'Baetz',        'desired_interval':'1'},
    # {'family':'Moreau',       'family_name':'Moreau',       'desired_interval':'1'},
    # {'family':'de_Roux',      'family_name':'de Roux',      'desired_interval':'1'},
    # {'family':'Stasiak',      'family_name':'Stasiak',      'desired_interval':'1'},
    # {'family':'Gregor',       'family_name':'Gregor',       'desired_interval':'1'},
    # {'family':'Roloff',       'family_name':'Roloff',       'desired_interval':'7'},
# ]).execute()    

Child.insert_many([
    # {'name':'NOT_ASSIGNED', 'family':'NOT_ASSIGNED'}, # 'empty' child
    # {'name':'Valentina',    'family':'Weber'},
    # {'name':'Paula',        'family':'Riedel'},
    # {'name':'Theo',         'family':'Ehmann'},
    # {'name':'Joas',         'family':'Tolonen'},
    # {'name':'Lotta',        'family':'Geffert'},
    # {'name':'Elli',         'family':'Tolonen'},
    # {'name':'Elio',         'family':'Nussbaum'},
    # {'name':'Penelope',     'family':'Baetz'},
    # {'name':'Marine',       'family':'Moreau'},
    # {'name':'Rosa',         'family':'de_Roux'},
    # {'name':'Jacek',        'family':'Stasiak'},
    # {'name':'Helena',       'family':'Stasiak'},
    # {'name':'Johannes',     'family':'Gregor'},
    # {'name':'Hanna',        'family':'Roloff'},
    # {'name':'Tim',          'family':'Roloff'},
# ]).execute()

NoServiceDay.insert_many([
     {'day':'2014-10-03'},
 ]).execute()

ServiceCategory.insert_many([
     {'name':'cooking'},
     {'name':'laundry',},
     {'name':'cleaning'},
 ]).execute()

Service.insert_many([
     {'name':'cooking_normal','category':'cooking', 'no_servants':'1', 'valid_on':'31', 'interval_min':'1',  'interval_max':'1'},
     {'name':'cooking_meat',  'category':'cooking', 'no_servants':'1', 'valid_on':'31', 'interval_min':'5',  'interval_max':'9'},
     {'name':'cooking_fish',  'category':'cooking', 'no_servants':'1', 'valid_on':'4',  'interval_min':'12', 'interval_max':'16'}, # Wednesdays
     {'name':'clean_kita',    'category':'cleaning','no_servants':'1', 'valid_on':'32', 'interval_min':'7',  'interval_max':'7'}, 
     {'name':'do_washing',    'category':'cooking', 'no_servants':'1', 'valid_on':'32', 'interval_min':'7',  'interval_max':'7'},
 ]).execute()

FamilyVacation.insert_many([
    {'family':'Stasiak', 'date':'2014-10-19'},
    {'family':'Stasiak', 'date':'2014-10-20'},
    {'family':'Stasiak', 'date':'2014-10-21'},
    {'family':'Stasiak', 'date':'2014-10-22'},
    {'family':'Stasiak', 'date':'2014-10-23'},
    {'family':'Stasiak', 'date':'2014-10-24'},
    {'family':'Stasiak', 'date':'2014-10-25'},
    {'family':'Stasiak', 'date':'2014-10-26'},
    {'family':'Stasiak', 'date':'2014-10-27'},
    {'family':'Stasiak', 'date':'2014-10-28'},
    {'family':'Stasiak', 'date':'2014-10-29'},
    {'family':'Stasiak', 'date':'2014-10-30'},
    {'family':'Stasiak', 'date':'2014-10-31'},
    {'family':'Stasiak', 'date':'2014-11-01'},
    {'family':'Stasiak', 'date':'2014-11-02'},
    
    {'family':'Geffert', 'date':'2014-10-02'},
    {'family':'Geffert', 'date':'2014-10-03'},
    {'family':'Geffert', 'date':'2014-10-19'},
    {'family':'Geffert', 'date':'2014-10-20'},
    {'family':'Geffert', 'date':'2014-10-21'},
    {'family':'Geffert', 'date':'2014-10-22'},
    {'family':'Geffert', 'date':'2014-10-23'},
    {'family':'Geffert', 'date':'2014-10-24'},
    {'family':'Geffert', 'date':'2014-10-25'},
    {'family':'Geffert', 'date':'2014-10-26'},
    {'family':'Geffert', 'date':'2014-10-27'},
    {'family':'Geffert', 'date':'2014-10-28'},
    {'family':'Geffert', 'date':'2014-10-29'},
    {'family':'Geffert', 'date':'2014-10-30'},
    {'family':'Geffert', 'date':'2014-10-31'},
    {'family':'Geffert', 'date':'2014-11-01'},
    {'family':'Geffert', 'date':'2014-11-02'},
    
    {'family':'Nussbaum', 'date':'2014-10-19'},
    {'family':'Nussbaum', 'date':'2014-10-20'},
    {'family':'Nussbaum', 'date':'2014-10-21'},
    {'family':'Nussbaum', 'date':'2014-10-22'},
    {'family':'Nussbaum', 'date':'2014-10-23'},
    {'family':'Nussbaum', 'date':'2014-10-24'},
    {'family':'Nussbaum', 'date':'2014-10-25'},
    {'family':'Nussbaum', 'date':'2014-10-26'},
    {'family':'Nussbaum', 'date':'2014-10-27'},
    {'family':'Nussbaum', 'date':'2014-10-28'},
    {'family':'Nussbaum', 'date':'2014-10-29'},
    {'family':'Nussbaum', 'date':'2014-10-30'},
    {'family':'Nussbaum', 'date':'2014-10-31'},
    {'family':'Nussbaum', 'date':'2014-11-01'},
    {'family':'Nussbaum', 'date':'2014-11-02'},
    
    {'family':'Moreau', 'date':'2014-10-23'},
    {'family':'Moreau', 'date':'2014-10-24'},
    {'family':'Moreau', 'date':'2014-10-25'},
    {'family':'Moreau', 'date':'2014-10-26'},
    {'family':'Moreau', 'date':'2014-10-27'},
    {'family':'Moreau', 'date':'2014-10-28'},
    
    {'family':'Gregor', 'date':'2014-10-20'},
    {'family':'Gregor', 'date':'2014-10-21'},
    
]).execute()

