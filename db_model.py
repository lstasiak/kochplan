from peewee import *
import datetime


db = MySQLDatabase('kochplan_atze', user='root',passwd='st1203',charset='utf8mb4')


# DB classes
class BaseModel(Model):
    # A base model that will use our MySQL database
    class Meta:
        database = db


class Family(BaseModel):
    # general family info
    family = CharField(primary_key=True)                  # unique (primary key) family descriptor -  usually == surname,
                                                          # in case of conflict e.g. combination of surname and year of joining kita
    family_name = CharField()                             # surname
    desired_interval = IntegerField(default=0)            # if more than 1 child active, what is the desired interval (in days) between services
                                                          # assigned to these children


class FamilyVacation(BaseModel):
    # family names and their vacation dates
    # Because we have not specified a primary key (PrimaryKeyField()), peewee will automatically
    # add an auto-incrementing integer primary key field named id.
    # auto-id
    family = ForeignKeyField(Family, related_name='vacations')
    date = DateField()


class Child(BaseModel):
    # children
    # auto-id
    name = CharField()
    family = ForeignKeyField(Family, related_name='children')
    join_date = DateField(default=datetime.date.today)         # date of joining kita
    left_date = DateField(default=datetime.date(2099,1,1))     # date of leaving kita


class NoServiceDay(BaseModel):
    # periods when none of the services required: holidays, Baitz-trip etc.,
    # auto-id
    day = DateField(unique=True)


class ServiceCategory(BaseModel):
    # cooking, washing, cleaning - to assure only one service of a given category per day
    # auto-id
    name = CharField(primary_key=True)


class Service(BaseModel):
    # specific service type from a category, e.g. cooking normal, cooking meat, cooking fish
    # auto-id
    name = CharField()
    category = ForeignKeyField(ServiceCategory, related_name='services')
    no_servants = IntegerField(default=1)   # how many people required to do service, e.g. cooking 1, weekly washing 2
    valid_on = IntegerField(default=31)     # BitMask: 1-Monday, 2-Tuesday, 4-Wed, 8-Thu, 16-Fri, 32-Sat, 64-Sun, weekdays: 31, weekend: 96
    interval_max = IntegerField(default=1)
    interval_min = IntegerField(default=1)  # this logically "overrides" settings of the category, e.g. cooking as general
                                            # is done every day, but cooking meat once a week


class ChildServiceDeactivation(BaseModel):
    # some children may be deactivated for some services, e.g. second child of a board member is excluded from cleaning service
    # auto-id
    child = ForeignKeyField(Child, related_name='service deactivations')
    category = ForeignKeyField(ServiceCategory, related_name='deactivated children')
    is_active = BooleanField(default=False)


class ChildServiceAssignment(BaseModel):
    # service assignments (all, no need to remove any rows from this table)
    # auto-id
    child = ForeignKeyField(Child, related_name='service assignments')
    service = ForeignKeyField(Service, related_name='child assignments')
    date = DateField()
