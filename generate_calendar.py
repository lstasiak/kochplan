# -*- coding: utf-8 -*-
import argparse
import calendar
import getopt
import locale
import logging
import random
import sys

from calendar import month_name, day_name, day_abbr
from operator import itemgetter, attrgetter

from reportlab.graphics.shapes import Drawing
from reportlab.lib import colors
from reportlab.lib.pagesizes import *
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch
from reportlab.platypus import Paragraph, SimpleDocTemplate, Table, TableStyle, Image

from peewee import *
from db_model import *

__author__ = "Lukasz Stasiak"
__contact__ = ""

#: logger instance
logger = None


def get_first_last_of_month(year, month):
    first_day = datetime.date(year, month, 1)
    last_day = datetime.date(year, month, calendar.monthrange(year, month)[1])
    return first_day, last_day

def get_month_name(month_no, locale):
    with calendar.TimeEncoding(locale) as encoding:
        s = month_name[month_no]
        if encoding is not None:
            s = s.decode(encoding)
        return s

def get_day_name(day_no, locale, short=False):
    with calendar.TimeEncoding(locale) as encoding:
        if short:
            s = day_abbr[day_no]
        else:
            s = day_name[day_no]
        if encoding is not None:
            s = s.decode(encoding)
        return s        

# check if the requested service is valid for a given day and not yet assigned
# returns Service or None
def service_free_for_assignment(service, date):
    null_child = get_empty_child_record()
    parent_category = service.category
    # check if the given day is a service day

    # check if service allowed on a requested day
    if (service.valid_on & (1<<date.weekday())) == 0:
        logger.info('Service named {} is not allowed on {}'.format(service.name, get_day_name(date.weekday(), "english")))
        return False

    # check if any assignment already exist for this day and given category
    assignments_on_day = ChildServiceAssignment.select().where(ChildServiceAssignment.date==date)
    for assignment in assignments_on_day:
        # print 'Assignment found: child', assignment.child.name, 'to', assignment.service.name, 'on', assignment.date
        if assignment.child != null_child and assignment.service.category == parent_category:
            logger.info('Service of category {} is already assigned on {}'.format(parent_category.name, date))
            return False

    # check if interval from the last service of this type is big enough
    last_service = ChildServiceAssignment.select().where(ChildServiceAssignment.service==service).order_by(
                                                         ChildServiceAssignment.date.desc()).first()
    if last_service != None and date - last_service.date < datetime.timedelta(days=service.interval_min):
        logger.info('Service too early')
        return False

    return True

def get_last_assignment(child,service):
    # get last assignment for a given service or a "higher difficulty" service of the same category (decided by interval_min)
    services = service.category.services.where(Service.interval_min >= service.interval_min)
    last_assignments = []
    
    for service_iter in services:
        assignment = ChildServiceAssignment.select().where(ChildServiceAssignment.child==child,
                                                                ChildServiceAssignment.service==service_iter).order_by(
                                                                ChildServiceAssignment.date.desc()).first()
        if not assignment == None:
            logger.info('Last assignment for child {}, service {}, was on {}'.format(child.name, service_iter.name, assignment.date))                                                                            
            last_assignments.append(assignment.date)
    
    if not last_assignments:
        return child.join_date
    else:
        last_assignments.sort(reverse=True) # sort descending
        logger.info('Returning last assignment date on {}'.format(last_assignments[0]))
        return last_assignments[0]

def is_child_deactivated(child, service_category):
    if ChildServiceDeactivation.select().where(ChildServiceDeactivation.child==child,
                                               ChildServiceDeactivation.category==service_category,
                                               ChildServiceDeactivation.is_active==False).exists():
        return True
    
    return False
    
        
def find_candidate_for_service_day(service, date):
    logger.info('')
    logger.info('Looking for candidate for {} on {}'.format(service.name, date))

    # Check if there is a desired service planned for the given day.
    if not ChildServiceAssignment.select().where(ChildServiceAssignment.service == service,
                                                 ChildServiceAssignment.date == date).exists():
        logger.info('There is no service {} planned on {} , so I cannot look for a candidate for it'.format(service.name, date))
        return None

    # CHECKS PASSED, NOW FIND CANDIDATE
    parent_category = service.category
    last_service_pro_child = []
    children = Child.select().join(Family).where(Family.family_name!='NOT_ASSIGNED') # all 'real' children

    for child in children:
        if is_child_deactivated(child, parent_category):
            logger.info('{} is deactivated for {}'.format(child.name, service.name))
        elif child.left_date < date:
            logger.info('{} has left "Atze und Co." on {}'.format(child.name, child.left_date))
        elif child.join_date > date:
            logger.info('{} has not joined "Atze und Co." yet, planned on {}'.format(child.name, child.join_date))
        else:
            # find last date the child did the given service (service, category is not enough)
            last_assignment = get_last_assignment(child, service)
            last_service_pro_child.append([child, last_assignment])

    last_service_pro_child = sorted(last_service_pro_child, key=itemgetter(1))

    # first in the list is the one who has not taken this service for the longest time,
    # let us check if he/she is available on that day (vacation), if yes - take him/her
    selected_candidate = None
    non_vacation_candidates = []
    for candidate_date in last_service_pro_child:
        # print 'CHECKING CAND', candidate_date[0].name, 'for', date
        # check if the given day is not vacation day (+/- 1 day as buffer)
        if not is_candidate_on_vacation(candidate_date[0], date):
            # print candidate_date[0].name, 'is not on vacation'
            # select a first candidate, who is not on vacation; in case all others have services assigned
            # too densly, we can at least take the first one who is not on vacation (even if needs to work too much)
            # candidate_date[0] is child, candidate_date[1] is last assignment
            non_vacation_candidates.append([candidate_date[0], abs(candidate_date[1] - date)])
            if not is_candidate_services_too_dense(candidate_date[0], parent_category, date):
                # this one in not on vacation and not too densly serving, select him!
                if selected_candidate is None:
                    # print 'NOT TOO DENSE IS', candidate_date[0].name
                    selected_candidate = candidate_date[0]
                if is_brothers_rule_satisfied(candidate_date[0], parent_category, date):
                    # try to find someone, for whom the brothers/sisters rule can be satisfied
                    # print 'BROTHER RULE', candidate_date[0].name
                    return candidate_date[0]

    # we have still not returned a valid candidate, for whom brothers/sisters rule cannot be satisified,
    # ignore the rule and take the first one, who does not work too much, if it is also not possible
    # just pick up the first candidate, not being on holidays
    if selected_candidate is not None:
        return selected_candidate
    elif non_vacation_candidates: # non_vacation_candidates is not empty
        print non_vacation_candidates
        non_vacation_candidates = sorted(non_vacation_candidates, key=itemgetter(1), reverse=True)
        print non_vacation_candidates
        logger.info('Not enough candidates to work, therefore {} must serve so often!'.format(non_vacation_candidates[0][0].name))
        return non_vacation_candidates[0][0]

    # if you're here, it means there is no candidate available at all! this should never happen!
    logger.info('HELP!: no candidate for service! How can it be?')
    return None

# check if the given date is not vacation day (+/- 'days_buffer' days as buffer) for candidate
def is_candidate_on_vacation(candidate, date, days_buffer=1):
    if FamilyVacation.select().where(FamilyVacation.family==candidate.family,
                                     FamilyVacation.date >= date-datetime.timedelta(days=days_buffer),
                                     FamilyVacation.date <= date+datetime.timedelta(days=days_buffer)).exists():
        logger.info('Checking if {} is on {} on vacation, result: true'.format(candidate.name,date))
        return True
    else:
        logger.info('Checking if {} is on {} on vacation, result: false'.format(candidate.name,date))
        return False


# simple rule to avoid assigning services of one category too often to a given candidate,
# e.g. cooking fish on Monday and cooking normal on Wednesday
def is_candidate_services_too_dense(candidate, category, date):

    # assume we want minimal span between services of the same category of at least 12 days (==2 weeks of cooking services)
    min_interval = 12
    
    if has_active_brothers(candidate, category, date):
        min_interval = 28 # a new 'solidarity' rule for families with more than 1 child in Kita, introduced during Elternabend on 6.11.2017
                          # min_interval is related to calendar days and not working days

    for service in category.services:
        last_assignment = get_last_assignment(candidate, service)
        # use 'abs' to consider future assignments as well
        if abs(date - last_assignment) < datetime.timedelta(days=min_interval):
            logger.info('{} is serving too often: last assignment was {}, now planning for {}'.format(candidate.name, last_assignment, date))
            return True # do not put too many duties on this candidate

    return False # the candidate has not done anything from this category for long time, let him do it now


# empty child record to be used in DB meaning no child assigned yet
def get_empty_child_record():
    def_family = Family.select().where(Family.family_name=='NOT_ASSIGNED').first()
    return def_family.children.first()

def is_nonservice_day(day_date):
    if NoServiceDay.select().where(NoServiceDay.day==day_date).exists():
        return True
    else:
        return False

def get_service_for_day(day_date, category):
    if is_nonservice_day(day_date):
        return None

    for service in category.services.where(Service.valid_on>0, Service.no_servants>0).order_by(Service.valid_on, Service.interval_max.desc()):
        if service_free_for_assignment(service, day_date):
            # try to make use of the whole span of the interval, by checking if one still may postpone it a little bit
            last_service = ChildServiceAssignment.select().where(ChildServiceAssignment.service==service).order_by(
                                                         ChildServiceAssignment.date.desc()).first()
            if last_service == None or day_date - last_service.date >= datetime.timedelta(days=service.interval_max):
                # it is high time to do it
                return service
            else:
                # we can still postpone, let us decide at random about it (chances for postponing proportional to free interval span),
                # but we have also to be sure that postponing for one day is allowed, otherwise we risk, that the service
                # will not be assigned at all: therefore postpone one day or not at all
                span_free = service.interval_max - (day_date - last_service.date).days
                if span_free < 0:
                    span_free = 0
                # if random.randint(0,span)==0 or not service_free_for_assignment(service, day_date+datetime.timedelta(days=1)):
                next_day = day_date+datetime.timedelta(days=1)
                if random.randint(0,span_free)==0 or is_nonservice_day(next_day) or not service_free_for_assignment(service, next_day):
                    # random says do it today OR random says do it tomorrow, but tomorrow it is not allowed -> so better today :-(
                    return service

    return None

def generate_services(date_from, date_to, category):

    null_child = get_empty_child_record()

    day = date_from
    while day <= date_to:
        service = get_service_for_day(day, category)
        if service is not None:
            for i in range(0, service.no_servants):
                ChildServiceAssignment.insert(service=service,date=day,child=null_child).execute()
        day = day + datetime.timedelta(days=1)

    return None

def sort_services_planned(x,y):
    # sort services planned: by interval_min descending (the most rare services to be assigned first), by date ascending
    if (x.service.interval_min > y.service.interval_min):
        return -1 # x's interval_min is greater, so place x before y, meaning "smaller" in our sorting logic
    elif (x.service.interval_min < y.service.interval_min):
        return 1
    else: # else compare dates
        return (x.date - y.date).days
    
def assign_servants(date_from, date_to):
    null_child = get_empty_child_record()
    reread_db = True

    while(reread_db):
        all_services_planned_db = ChildServiceAssignment.select().where(ChildServiceAssignment.date >= date_from,
                                                                    ChildServiceAssignment.date <= date_to).join(Service).order_by(Service.interval_min.desc(), Service.valid_on)
        # sort planned services so, that most rarely taken services are assigned first (higher priority), and services with the same priority are sorted by date (otherwise,
        # if the order is random, some candidates may be assigned to "densely")
        all_services_planned = sorted( list(all_services_planned_db), cmp=sort_services_planned )
        reread_db = False
                     
        for service_planned in all_services_planned:
            if service_planned.child == null_child:
                # do assignment only if there is nobody assigned yet
                candidate = find_candidate_for_service_day(service_planned.service,service_planned.date)
                if candidate is None:
                    logger.info('Houston! We have a problem - we should always have a candidate')
                    return None
                else:
                    # candidate found, update DB record
                    logger.info('Service {} on {} to be assigned to {}'.format(service_planned.service.name, service_planned.date, candidate.name))
                    service_planned.child = candidate
                    service_planned.save()

                    # if the candidate has sisters/brothers try to assign them as well accordingly to sisters/brothers-rule
                    if (assign_brothers(service_planned, candidate) == True):
                        # print 'Brother ASSIGNED'
                        # we did assignment in another record of DB, we need to do DB reread
                        reread_db = True
                        break

def is_desired_interval_valid_for_category(desired_interval, category):
    desired_interval_too_small = True
    for service in category.services:
        if desired_interval >= service.interval_min:
            desired_interval_too_small = False
    
    return not desired_interval_too_small
                        
def is_brothers_rule_satisfied(child, category, date):
    # since we fulfill the brothers rule in the future from the given date,
    # we now simply assume it can always be satisifed (worse or better),
    # we do not check if the desired interval date is actually a service day - can be extended,
    # if exteding this check in the future take care of days at the end of the month - previously it resulted in children with desired
    # interval of 7 days not to be asigned in last week of the month (since desired day for brother
    # was always on no service day, i.e. a day from the next month, which was not yet defined as a service day)
    # return True
    
    null_child = get_empty_child_record()
    brothers_counter = 0

    logger.info('Checking if brothers rule can be possibly satisfied for child {}, service category {}.'.format(child.name, category.name))
    
    for family_child in child.family.children:
        if family_child != child and not is_child_deactivated(family_child, category) \
                                 and not family_child.left_date < date \
                                 and not family_child.join_date > date:
            brothers_counter = brothers_counter + 1
            if brothers_counter > 1:
                logger.info('WARNING: {} brothers/sisters detected for {}. Actually only 1 brother/sister is currently supported.'.format(brothers_counter, family_child.name))
    
    if brothers_counter == 0: # no need to check, return
        return True
        
    desired_interval = child.family.desired_interval
    
    # if desired interval < minimal interval ignore 'brothers rule'
    # and return True, however it is clear this rule can never be satisifed
    # this is a kind of workaround - initially 'brothers rule' was planned for cooking only
    # to avoid skipping assignments to laundry and cleaning when desired_interval=1 we added this condition
    # - should be improved in the future (or brothers rule removed?)
    if not is_desired_interval_valid_for_category(desired_interval, category):
        return True
            
    # check desired interval and +/- 50% around it
    sign = -1;
    i = 1;            
    date_for_brother = date + datetime.timedelta(days=desired_interval)
            
    while (date_for_brother - date > datetime.timedelta(days=0)) and (date_for_brother - date >= datetime.timedelta(days=desired_interval/2)) and (date_for_brother - date <= datetime.timedelta(days=3*desired_interval/2)): # try desired interval +/- max 0.5 of the desired interval
        service_planned = ChildServiceAssignment.select().where(ChildServiceAssignment.date == date_for_brother).first()
        # if no service planned we assume the brothers rule can be satisfied (possibly on another day, reason for service_planned = None: 
        # no_service_day or e.g. at the end of the month no services generated yet for the next month)
        # if there is a service planned, but no assignment yet the brothers rule can be clearly satisifed
        if service_planned == None or (service_planned.child == null_child):
            return True
        date_for_brother = date + datetime.timedelta(days=desired_interval+sign*i)
        if (sign > 0):
            i = i+1
        sign = -sign    

    return False

def has_active_brothers(candidate, service_category, date):
    # checking if there are brothers or sisters active on the date for the service_category
    brother_count = 0
    for brother in candidate.family.children:
        if brother != candidate and not brother.left_date < date and not brother.join_date > date \
        and not is_child_deactivated(brother, service_category):
            brother_count += 1

    if brother_count > 0:
        return True
    else:
        return False
        

def assign_brothers(service_planned, candidate):
    ret_val = False
    date = service_planned.date
    category = service_planned.service.category
    null_child = get_empty_child_record()
    # if the desired interval does not apply to the given service, do not even try to assign brothers
    if not is_desired_interval_valid_for_category(candidate.family.desired_interval, category):
        return ret_val
        
    for brother in candidate.family.children: # 'brother' is brother or sister
        if brother != candidate and not brother.left_date < date and not brother.join_date > date \
        and not is_child_deactivated(brother, category):
            # brother or sister found: try to assign family rule, we try assignments only forward in calendar
            # rule is simply based on checking if brother/sister is not serving too often, we currently skip
            # checking what kind of service will be assigned to brother/sister (maybe it will be necessary, if it
            # appears, that special services (meat, fish) appear too often then
            logger.info('{} has sister/brother: {}, trying to assign services for them wrt desired interval...'.format(candidate.name, brother.name))
            desired_interval = candidate.family.desired_interval
            sign = -1;
            i = 1;            
            date_for_brother = date + datetime.timedelta(days=desired_interval)
            
            while (date_for_brother - date > datetime.timedelta(days=0)) and (date_for_brother - date >= datetime.timedelta(days=desired_interval/2)) and (date_for_brother - date <= datetime.timedelta(days=3*desired_interval/2)): # try desired interval +/- max 0.5 of the desired interval
                logger.info('Checking date {}'.format(date_for_brother))
                service_planned = ChildServiceAssignment.select().where(ChildServiceAssignment.date == date_for_brother).first()
                if (service_planned is not None and (service_planned.child == null_child) and 
                not is_candidate_services_too_dense(brother, service_planned.service.category, date_for_brother) and
                not is_candidate_on_vacation(brother, date_for_brother)):
                    # do actual assignment
                    logger.info('Assigning sister/brother service {} to {} on {} ({})'.format(service_planned.service.name, brother.name, date_for_brother, service_planned.date))
                    service_planned.child = brother
                    service_planned.save()
                    ret_val = True # we assigned something "in the future"
                    break
                date_for_brother = date + datetime.timedelta(days=desired_interval+sign*i)
                if (sign > 0):
                    i = i+1
                sign = -sign
    return ret_val


# takes a list of dates (date objects) and returns a string describing date range, e.g.
# from [2014-10-01, 2014-10-02,...,2014-10-10] does "01.10.2014-10.10.2014"
def get_dates_range_string(dates):
    dates.sort()
    date_range_string = ''
    date_range_cntr = 0 # counts days within one continuous date span

    for idx, date in enumerate(dates):
        if idx == 0:
            date_range_string = date.strftime("%d.%m.%Y")
            continue

        if date - dates[idx-1] <= datetime.timedelta(days=1): # next day or the same day repeated
            within_range = True
            date_range_cntr += 1
        else:
            within_range = False

        if not within_range:
            # continuous date span ended, update string
            if date_range_cntr > 0:
                # close previous span
                date_range_string += ' - ' + dates[idx-1].strftime("%d.%m.%Y")
            # add current date
            date_range_string += ', ' + date.strftime("%d.%m.%Y")
            date_range_cntr = 0

    # end of the loop, but still some date span to be reported?
    if date_range_cntr > 0:
        date_range_string += ' - ' + date.strftime("%d.%m.%Y")

    return date_range_string

def get_vacations_paragraph(target_year, target_month):
    first_day_of_month, last_day_of_month = get_first_last_of_month(target_year, target_month)

    lines_cntr = 0
    paragraph_text = ''
    families = Family.select()
    for family in families:
        vacations = FamilyVacation.select().where(FamilyVacation.family==family,
                                                  FamilyVacation.date >= first_day_of_month,
                                                  FamilyVacation.date <= last_day_of_month)
        if vacations.count() == 0:
            continue

        vac_dates = []
        for vac in vacations:
            vac_dates.append(vac.date)
        
        children_in_family = ''  # family descriptor
        for child in family.children:
            if child.left_date > first_day_of_month:
                children_in_family += ', ' + child.name
        paragraph_text += '<br/>' + children_in_family[2:] + ': ' + get_dates_range_string(vac_dates) # '<br/>' as break line for reportlab    
        lines_cntr += 1

    style = getSampleStyleSheet()
    return (Paragraph(paragraph_text,style["Normal"]), lines_cntr)

def print_calendar(target_year, target_month, print_vacation_par=False):
    null_child = get_empty_child_record()
    # images
    img_fish = Image('./img/fish.jpg', height=0.67*cm, width=1.17*cm)
    img_fish.vAlign = 'TOP'
    img_meat = Image('./img/meat2.jpg', height=0.75*cm, width=0.75*cm)
    img_meat.vAlign = 'TOP'
    img_cleaning = Image('./img/cleaning.jpg', height=1.30*cm, width=1.17*cm)
    img_cleaning.vAlign = 'TOP'
    img_laundry = Image('./img/laundry.jpg', height=1.30*cm, width=1.15*cm)
    img_laundry.vAlign = 'TOP'
    img_noservice = Image('./img/closed.jpg', height=1.5*cm, width=2.122*cm)
    img_noservice.vAlign = 'MIDDLE'
    img_vacation = Image('./img/vacation2.jpg', height=2.0*cm, width=1.70*cm)
    img_vacation.hAlign = 'LEFT'


    # get calendar data and fill it in
    monthcalendar = list(calendar.Calendar().itermonthdates(target_year, target_month))
    no_of_week_rows = len(monthcalendar)/7  # monthcalendar due to using itermonthdates is always of length 35
    lang = "german"  # german, english, polish...
    cal_data = [[get_day_name(0,lang), get_day_name(1,lang), get_day_name(2,lang), get_day_name(3,lang),
                 get_day_name(4,lang), get_day_name(5,lang), get_day_name(6,lang,True)]]

    # create data and style for the schedule
    style = TableStyle([
            ('FONT', (0, 0), (-1, -1), 'Helvetica'),
            ('FONT', (0, 0), (-1, 0), 'Helvetica-Bold'),
            ('FONTSIZE', (0, 0), (-1, -1), 12),
            ('TEXTCOLOR', (5, 0), (5, -1), colors.blue),        # Saturday blue
            ('TEXTCOLOR', (6, 0), (6, -1), colors.red),         # Sunday red
            ('INNERGRID', (0, 0), (-1, -1), 0.5, colors.black),
            ('LINEBELOW', (0, 0), (-1, 0), 5.0, colors.black),
            ('BOX', (0, 0), (-1, -1), 0.5, colors.black),
            ('ALIGN', (0, 0), (-1, 0), 'CENTER'),  # aligment center for the first row (0) and all columns (from 0 to the last, i.e. -1)
            ('ALIGN', (0, 1), (-1, -1), 'LEFT'),   # left alignment for all rows starting from the second row (1)
            # ('ALIGN', (0, 2), (-3, 2), 'CENTER'),   # center alignment for 2nd, 4th, 6th.. row, except for 2 last columns, overwrites previous left alignment
            # ('ALIGN', (0, 4), (-3, 4), 'CENTER'),
            # ('ALIGN', (0, 6), (-3, 6), 'CENTER'),
            # ('ALIGN', (0, 8), (-3, 8), 'CENTER'),
            # ('ALIGN', (0, 10), (-3, 10), 'CENTER'),
            ('ALIGN', (-3, 2), (-1, 2), 'RIGHT'),   # center alignment for 2nd, 4th, 6th.. row, only 2 last columns, overwrites previous left alignment
            ('ALIGN', (-3, 4), (-1, 4), 'RIGHT'),
            ('ALIGN', (-3, 6), (-1, 6), 'RIGHT'),
            ('ALIGN', (-3, 8), (-1, 8), 'RIGHT'),
            ('ALIGN', (-3, 10), (-1, 10), 'RIGHT'),
            ('VALIGN', (0, 0), (-1, -1), 'TOP')
        ])
    for row in range(0, no_of_week_rows):
            style.add('BACKGROUND',(0,row*2+1),(-1,row*2+1),colors.yellow) # yellow row with date and servant name

    day_idx = -1
    rowHeights=[]
    while day_idx < len(monthcalendar)-1:

        row_head = []
        row = []
        for i in range(1,8): # for a whole week (7 days always) == one row in schedule
            day_idx += 1
            appendix = ''
            content = []
            assignments = ChildServiceAssignment.select().where(ChildServiceAssignment.date==monthcalendar[day_idx])
            
            if assignments.count() == 0 and NoServiceDay.select().where(NoServiceDay.day==monthcalendar[day_idx]).exists():
                # this is a day without service
                # print 'Day', monthcalendar[day_idx], 'is no service day'
                content.append(img_noservice)
            else:
                if assignments.count() > 1:
                    appendix = '\n \n'
                for assign in assignments:
                    if assign.child != null_child:
                        appendix = appendix + assign.child.name + '\n'
                    if assign.service.name == 'cooking_fish' and img_fish not in content:
                        content.append(img_fish)
                    if assign.service.name == 'cooking_meat' and img_meat not in content:
                        content.append(img_meat)
                    if assign.service.name == 'clean_kita' and img_cleaning not in content:
                        content.append(img_cleaning)
                    elif assign.service.name == 'do_washing' and img_laundry not in content:
                        content.append(img_laundry)
            
            row_head.append((monthcalendar[day_idx].day).__str__() + ' ' + appendix)
            row.append(content)
            if monthcalendar[day_idx].month != target_month: # not a current month, draw it grey
                current_row = (day_idx // 7)*2 + 1
                current_col = day_idx % 7
                style.add('BACKGROUND', (current_col, current_row), (current_col, current_row), colors.lavender)

        cal_data.append(row_head)
        cal_data.append(row)
        rowHeights.append(0.7*cm) # row header (day name)
        rowHeights.append(2.3*cm) # row field (for parents' input)


    # create schedule for final pdf
    # rowHeights = no_of_week_rows*[0.7*cm,2.3*cm]
    rowHeights.insert(0, 1*cm)
    schedule = Table(cal_data, colWidths=(4*cm,4*cm,4*cm,4*cm,4*cm,4*cm,4*cm), rowHeights=rowHeights)
    schedule.setStyle(style)

    # create header of the document
    style = getSampleStyleSheet()
    title = Paragraph(get_month_name(target_month,lang) + ' ' + str(target_year) + '<br/>http://atze.kochplan.eu/', style["Title"])

    # create footprint of pdf (vacations)
    (vac_par, vac_lines) = get_vacations_paragraph(target_year, target_month)
    if print_vacation_par == True and vac_lines > 0:
        vacations = [[img_vacation,vac_par]]
    else:
        vacations = [['','']]
    style = TableStyle([('ALIGN', (0, 0), (-1, -1), 'LEFT'),
                        ('VALIGN',(0, 0), (-1, -1), 'MIDDLE')])
    vacations_tab = Table(vacations, colWidths=(2*cm,22*cm))
    vacations_tab.setStyle(style)

    #create pdf all this
    doc = SimpleDocTemplate('calendar.pdf', pagesize=landscape(A4),
                             title='Atze & Co. Kochplan', author='autogenerated (generator by LS 2014)',
                             topMargin = 0.15*cm, bottomMargin = 0.18*cm)
    doc.build([title, schedule, vacations_tab])


#//////////////////////////////////////////////////////////////////////////////
def main(argv):
    global logger

    argParser = argparse.ArgumentParser(description='')
    argParser.add_argument('-gc', '--generate_cook',    dest='generate_cook_active',    action="store_true", default=False, help="generate cooking services (database will be updated)")
    argParser.add_argument('-gp', '--generate_clean',   dest='generate_clean_active',   action="store_true", default=False, help="generate cleaning (Putzen) services (database will be updated)")
    argParser.add_argument('-gl', '--generate_laundry', dest='generate_laundry_active', action="store_true", default=False, help="generate laundry services (database will be updated)")
    argParser.add_argument('-g', '--generate_all',      dest='generate_all_active',     action="store_true", default=False, help="generate all services (database will be updated)")
    argParser.add_argument('-a', '--assign',   dest='assign_active',   action="store_true", default=False, help="assign servants (database will be updated)")
    argParser.add_argument('-p', '--print',    dest='print_active',    action="store_true", default=False, help="print schedule (generate pdf)")
    argParser.add_argument('-y', '--year',     dest='year',  type=int, required=True, help="year of interest")
    argParser.add_argument('-m', '--month',    dest='month', type=int, required=True, help="month of interest")
    argParser.add_argument('-v', '--vacation', dest='print_vacation_par', action="store_true", default=False, help="print vacation plans summary in the schedule")
    argParser.add_argument('-l', '--loglevel', dest='loglevel', default='INFO', help='logging level: INFO, DEBUG, ERROR; default: INFO')
    args = argParser.parse_args()

    initLogger(args.loglevel)
    logger = logging.getLogger('')
    if args.generate_all_active:
        args.generate_cook_active    = True
        args.generate_clean_active   = True
        args.generate_laundry_active = True
    
    # Print infos on incomplete command
    if not (args.generate_cook_active or args.generate_clean_active or args.generate_laundry_active or
            args.assign_active or args.print_active):
        logger.info('No action defined, I have nothing to do.')
        sys.exit(2)

    # Print summary on selected actions
    logger.info('Actions selected for month {} of year {}:'.format(args.month, args.year))
    if args.generate_cook_active:
        logger.info('   - generate cooking services')
    if args.generate_clean_active:
        logger.info('   - generate cleaning services')
    if args.generate_laundry_active:
        logger.info('   - generate laundry services')
    if args.assign_active:
        logger.info('   - assign servants')
    if args.print_active:
        logger.info('   - print schedule')

    # Take actual actions
    # timeit results before [0.025398367953044954, 0.02504199027904974, 0.024934404566153034]
    # timeit results after [0.013231627085275477, 0.012770494901346297, 0.012592483014543632]
    first_day_of_month, last_day_of_month = get_first_last_of_month(args.year, args.month)

    if args.generate_cook_active:
        cooking_category = ServiceCategory.select().where(ServiceCategory.name=='cooking').first()
        generate_services(first_day_of_month, last_day_of_month, cooking_category)
    if args.generate_clean_active:
        cleaning_category = ServiceCategory.select().where(ServiceCategory.name=='cleaning').first()
        generate_services(first_day_of_month, last_day_of_month, cleaning_category)
    if args.generate_laundry_active:
        laundry_category = ServiceCategory.select().where(ServiceCategory.name=='laundry').first()
        generate_services(first_day_of_month, last_day_of_month, laundry_category)
    if args.assign_active:
        assign_servants(first_day_of_month, last_day_of_month)
    if args.print_active:
        print_calendar(args.year, args.month, args.print_vacation_par)


def initLogger(loglevel):
    '''
    intialize the logger with loglevel etc

    :param loglevel: read the name of the variable
    '''
    filename = '{0}.log'.format(datetime.datetime.now().strftime("%Y%m%d_%H%M"))
    logging.basicConfig(filename=filename,
                        filemode='w',
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%d.%m %H:%M',
                        level=loglevel)

    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)


#//////////////////////////////////////////////////////////////////////////////
if __name__ == "__main__":
    main(sys.argv)
